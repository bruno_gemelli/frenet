<?php
/**
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @copyright Copyright (c) 2018 MagedIn. (http://www.magedin.com)
 *
 * @author    Bruno Gemelli <bruno.gemelli@magedin.com>
 */

namespace MagedIn\Frenet\Model;

class ServiceRepository implements \MagedIn\Frenet\Api\ServiceRepositoryInterface
{
    /**
     * @var string
     */
    const API_BASE_URI = 'http://api.frenet.com.br/';

    /**
     * @var string
     */
    const API_SHIPPING_QUOTE_URN = 'shipping/quote';

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_zendClientFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;


    /**
     * ServiceRepository constructor.
     *
     * @param Context $context
     */
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\HTTP\ZendClientFactory $zendClientFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_productRepository = $productRepository;
        $this->_zendClientFactory = $zendClientFactory;
        $this->_scopeConfig       = $scopeConfig;
    }

    /**
     * Handle Frenet API request for shipping quote
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $rateRequest
     *
     * @return mixed|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Http_Client_Exception
     */
    public function getShippingQuote(\Magento\Quote\Model\Quote\Address\RateRequest $rateRequest)
    {
        $apiBodyRequest = [
            'SellerCEP'             => $this->getConfigData('shipping/origin/postcode'),
            'RecipientCEP'          => $rateRequest->getDestPostcode(),
            'ShipmentInvoiceValue'  => $rateRequest->getPackageValueWithDiscount(),
            'ShippingItemArray'     => $this->_prepareQuoteItems($rateRequest),
        ];

        $response = $this->_request(
            self::API_SHIPPING_QUOTE_URN,
            $apiBodyRequest
        );

        if (!isset($response['ShippingSevicesArray'])) {
            //throw exception?
            //log?
            return null;
        }

        return $response['ShippingSevicesArray'];
    }


    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $rateRequest
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _prepareQuoteItems(\Magento\Quote\Model\Quote\Address\RateRequest $rateRequest)
    {
        $items = [];

        /** @var \Magento\Quote\Api\Data\CartItemInterface $item */
        foreach ($rateRequest->getAllItems() as $item) {

            /**
             * Skip bundle and configurable product types
             */
            if ($item->getProductType() != \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE) {
                continue;
            }

            $hasParent = ($item->getParentItemId()) ? true : false;

            $product        = $this->_productRepository->getById($item->getProductId());
            $parentProduct  = null;

            if ($hasParent) {
                $parentProduct        = $this->_productRepository->getById($item->getParentItem()->getProductId());
            }

            $items[] = [
                'Weight'    => $product->getWeight(),
                'Length'    => $product->getData('ts_dimensions_length'),
                'Height'    => $product->getData('ts_dimensions_height'),
                'Width'     => $product->getData('ts_dimensions_width'),
                'Quantity'  => ($hasParent) ? $item->getParentItem()->getQty() : $item->getQty(),
            ];
        }

        return $items;
    }


    /**
     * @param string $endpoint
     * @param array $data
     * @param string|null $method
     *
     * @return array
     * @throws \Zend_Http_Client_Exception
     */
    protected function _request(
        $endpoint,
        $data,
        $method = \Zend_Http_Client::POST
    )
    {
        /** @var \Magento\Framework\HTTP\ZendClient $client */
        $client = $this->_zendClientFactory->create();

        $client->setUri(self::API_BASE_URI.$endpoint);
        $client->setMethod($method);
        $client->setRawData(json_encode($data), 'application/json');
        $client->setHeaders(
            [
                'Content-Type' => 'application/json',
                'token'        => $this->getConfigData('carriers/magedinfrenet/token')
            ]
        );
        $client->setUrlEncodeBody(false);

        $response = $client->request();

        if (!$response->isSuccessful()) {
            //throw exception?
            //log?
            return null;
        }

        //@todo log??

        $bodyResponse = json_decode($response->getBody(), true);

        return $bodyResponse;
    }


    /**
     * @param $path
     *
     * @return mixed
     */
    public function getConfigData($path)
    {
        return $this->_scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}